package com.evgenivanovi.microservice.registry.domain.model;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Wither;

@Getter
@Wither
@Builder
public class DeviceFilter {

    private String vendor;
    private String model;

}
