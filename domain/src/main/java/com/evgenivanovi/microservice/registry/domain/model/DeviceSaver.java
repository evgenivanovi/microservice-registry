package com.evgenivanovi.microservice.registry.domain.model;

import reactor.core.publisher.Mono;

public interface DeviceSaver {

    Mono<DeviceInfo> saveDevice(DeviceInfo device);

}
