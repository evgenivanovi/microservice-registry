package com.evgenivanovi.microservice.registry.domain.model;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Wither;
import reactor.core.publisher.Mono;

@Getter
@Wither
@Builder
public class Device {

    private String id;
    private String ssn;
    private String vendor;
    private String model;
    private String mac;

    public Mono<Device> toMono() {
        return Mono.just(this);
    }

    public Device empty() {
        return Device.builder().build();
    }

    public Mono<Device> emptyMono() {
        return Device.builder().build().toMono();
    }

}
