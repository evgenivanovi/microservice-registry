package com.evgenivanovi.microservice.registry.domain.model;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Wither;
import reactor.core.publisher.Mono;

@Getter
@Wither
@Builder
public class DeviceInfo {

    private String id;
    private String ssn;
    private String vendor;
    private String model;
    private String mac;
    private String ip;
    private String netmask;

    public Mono<DeviceInfo> toMono() {
        return Mono.just(this);
    }

    public static DeviceInfo empty() {
        return DeviceInfo.builder().build();
    }

    public static Mono<DeviceInfo> emptyMono() {
        return DeviceInfo.builder().build().toMono();
    }

}
