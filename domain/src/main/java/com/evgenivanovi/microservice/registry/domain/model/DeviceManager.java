package com.evgenivanovi.microservice.registry.domain.model;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface DeviceManager {

    Flux<DeviceInfo> searchAllDevices();

    Flux<DeviceInfo> searchDevices(DeviceFilter deviceFilter);

    Mono<DeviceInfo> searchSingleDevice(String id);

    Mono<DeviceInfo> addDevice(Device device);

}
