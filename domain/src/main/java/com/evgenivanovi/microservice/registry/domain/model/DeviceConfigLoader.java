package com.evgenivanovi.microservice.registry.domain.model;

import reactor.core.publisher.Mono;

public interface DeviceConfigLoader {

    Mono<DeviceConfig> searchDeviceConfig(String ssn);

}
