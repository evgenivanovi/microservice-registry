package com.evgenivanovi.microservice.registry.domain.service;

import com.evgenivanovi.microservice.registry.domain.model.Device;
import com.evgenivanovi.microservice.registry.domain.model.DeviceConfig;
import com.evgenivanovi.microservice.registry.domain.model.DeviceConfigLoader;
import com.evgenivanovi.microservice.registry.domain.model.DeviceFilter;
import com.evgenivanovi.microservice.registry.domain.model.DeviceInfo;
import com.evgenivanovi.microservice.registry.domain.model.DeviceLoader;
import com.evgenivanovi.microservice.registry.domain.model.DeviceManager;
import com.evgenivanovi.microservice.registry.domain.model.DeviceSaver;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
public class DeviceManagerService implements DeviceManager {

    private final DeviceLoader deviceLoader;
    private final DeviceSaver deviceSaver;
    private final DeviceConfigLoader deviceConfigLoader;

    @Override
    public Flux<DeviceInfo> searchAllDevices() {
        return deviceLoader.searchAllDevices();
    }

    @Override
    public Flux<DeviceInfo> searchDevices(final DeviceFilter deviceFilter) {
        return deviceLoader.searchDevices(deviceFilter);
    }

    @Override
    public Mono<DeviceInfo> searchSingleDevice(final String id) {
        return deviceLoader.searchSingleDevice(id);
    }

    @Override
    public Mono<DeviceInfo> addDevice(final Device device) {
        return device.toMono()
                .zipWith(deviceConfigLoader.searchDeviceConfig(device.getSsn()))
                .filter(deviceTuple -> isEquivalentDevices(deviceTuple.getT1(), deviceTuple.getT2()))
                .map(deviceTuple -> DeviceManagerService.toDeviceInfo(deviceTuple.getT1(), deviceTuple.getT2()))
                .flatMap(deviceSaver::saveDevice);
    }

    private static DeviceInfo toDeviceInfo(final Device device, final DeviceConfig deviceConfig) {
        return DeviceInfo.builder()
                .ssn(deviceConfig.getSsn())
                .vendor(device.getVendor())
                .model(device.getModel())
                .mac(device.getMac())
                .ip(deviceConfig.getIp())
                .netmask(deviceConfig.getNetmask())
                .build();
    }

    private static boolean isEquivalentDevices(final Device device, final DeviceConfig deviceConfig) {
        return device.getSsn().equals(deviceConfig.getSsn());
    }

}
