package com.evgenivanovi.microservice.registry.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Wither;
import reactor.core.publisher.Mono;

@Getter
@Setter
@Wither
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeviceConfig {

    private String id;
    private String ssn;
    private String ip;
    private String netmask;

    public Mono<DeviceConfig> toMono() {
        return Mono.just(this);
    }

    public DeviceConfig empty() {
        return DeviceConfig.builder().build();
    }

    public Mono<DeviceConfig> emptyMono() {
        return DeviceConfig.builder().build().toMono();
    }

}
