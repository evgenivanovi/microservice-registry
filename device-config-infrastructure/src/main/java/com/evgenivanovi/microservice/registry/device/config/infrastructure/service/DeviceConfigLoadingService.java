package com.evgenivanovi.microservice.registry.device.config.infrastructure.service;

import com.evgenivanovi.microservice.registry.domain.model.DeviceConfig;
import com.evgenivanovi.microservice.registry.domain.model.DeviceConfigLoader;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Slf4j
@RequiredArgsConstructor
public class DeviceConfigLoadingService implements DeviceConfigLoader {

    private static final String BASE_URI = "/api/service/configuration";

    private final WebClient.Builder webClientBuilder;

    @Override
    public Mono<DeviceConfig> searchDeviceConfig(final String ssn) {
        return webClientBuilder.build()
                .get()
                .uri(searchSingleDeviceUri(ssn))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .retrieve()
                .bodyToMono(DeviceConfig.class);
    }

    private static String searchSingleDeviceUri(final String ssn) {
        return BASE_URI + "/" + ssn;
    }

    private static ExchangeFilterFunction log() {
        return ExchangeFilterFunction.ofRequestProcessor(
                req -> {
                    log.info("Request: {} {}", req.method(), req.url());
                    req.headers().forEach((name, values) -> values.forEach(value -> log.info("{}={}", name, value)));
                    return Mono.just(req);
                });
    }

}
