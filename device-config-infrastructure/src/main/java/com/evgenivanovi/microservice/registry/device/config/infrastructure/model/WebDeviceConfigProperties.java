package com.evgenivanovi.microservice.registry.device.config.infrastructure.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WebDeviceConfigProperties {

    private String address;
    private String port;

    public String getUrl() {
        return "http://" + address + ":" + port;
    }

}
