# Registry Microservice Description

*Registry Microservice* - stores a list of devices, allows you to add new devices. 

## Functional Description

- When adding a new device to the catalog, the user specifies the `manufacturer`, `model`, `serial number` and `MAC address` of the device. 
The Registry Microservice asks for the Configuration Microservice and for a given serial number retrieves the configuration - `IP address` and `subnet mask` for this device.
The resulting IP address and subnet mask values are stored in the Registry Microservice. 
- The `serial number` is unique. The application should not allow you to add multiple devices with the same serial number to the database.

---

## OAPI Description

| OAPI Function                                              | Headers                                 |                                          Body                                          | Description                                      |
|------------------------------------------------------------|-----------------------------------------|:--------------------------------------------------------------------------------------:|--------------------------------------------------|
| `GET /api/service/registry`                                | - Content-Type: application/stream+json |                                            -                                           | Load a list of all devices from the directory    |
| `GET /api/service/registry?vendor=#{value}&model=#{value}` | - Content-Type: application/stream+json |                                            -                                           | Load a list of devices by filters: vendor, model |
| `GET /api/service/registry/#{value}`                       | - Content-Type: application/json        |                                            -                                           | Load one device based on serial number.          |
| `POST /api/service/registry`                               | - Content-Type: application/json        | { "ssn": "Serial Number", "vendor": "Vendor", "model": "Model", "mac": "MAC address" } | Add a new device to the catalog                  |

---

## Device Description

The device in the catalog contains the following fields:

- `id` - identifier
- `ssn` - serial number
- `vendor` - manufacturer
- `model` - model
- `mac` - MAC address
- `ip` - IP address (has to get info from Microservice Configuration)
- `netmask` - subnetwork or subnet (has to get info from Microservice Configuration)

---