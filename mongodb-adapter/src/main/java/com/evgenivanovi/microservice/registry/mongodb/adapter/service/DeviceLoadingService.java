package com.evgenivanovi.microservice.registry.mongodb.adapter.service;

import com.evgenivanovi.microservice.registry.domain.model.DeviceFilter;
import com.evgenivanovi.microservice.registry.domain.model.DeviceInfo;
import com.evgenivanovi.microservice.registry.domain.model.DeviceLoader;
import com.evgenivanovi.microservice.registry.mongodb.adapter.model.DeviceDocument;
import com.evgenivanovi.microservice.registry.mongodb.adapter.model.DeviceIndex;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
public class DeviceLoadingService implements DeviceLoader {

    private final DeviceIndex deviceIndex;

    @Override
    public Flux<DeviceInfo> searchAllDevices() {
        return deviceIndex.searchDevices(new Query())
                .map(DeviceLoadingService::toDevice);
    }

    @Override
    public Flux<DeviceInfo> searchDevices(final DeviceFilter deviceFilter) {
        final Criteria criteria = DeviceLoadingService.toFilterCriteria(deviceFilter);
        return deviceIndex.searchDevices(Query.query(criteria))
                .map(DeviceLoadingService::toDevice);
    }

    @Override
    public Mono<DeviceInfo> searchSingleDevice(final String id) {
        final Criteria criteria = DeviceLoadingService.toIdCriteria(id);
        return deviceIndex.searchDevice(Query.query(criteria))
                .map(DeviceLoadingService::toDevice);
    }

    private static DeviceInfo toDevice(final DeviceDocument deviceDocument) {
        return DeviceInfo.builder()
                .id(deviceDocument.getId())
                .ssn(deviceDocument.getSsn())
                .vendor(deviceDocument.getVendor())
                .model(deviceDocument.getModel())
                .mac(deviceDocument.getMac())
                .ip(deviceDocument.getIp())
                .netmask(deviceDocument.getNetmask())
                .build();
    }

    private static Criteria toIdCriteria(final String id) {
        return Criteria
                .where("ssn").is(id);
    }

    private static Criteria toFilterCriteria(final DeviceFilter deviceFilter) {
        return Criteria
                .where("vendor").is(deviceFilter.getVendor())
                .and("model").is(deviceFilter.getModel());
    }

}
