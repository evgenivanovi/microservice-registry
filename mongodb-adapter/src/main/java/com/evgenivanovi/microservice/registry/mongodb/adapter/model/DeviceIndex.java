package com.evgenivanovi.microservice.registry.mongodb.adapter.model;

import org.springframework.data.mongodb.core.query.Query;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface DeviceIndex {

    Flux<DeviceDocument> searchDevices(final Query query);

    Mono<DeviceDocument> searchDevice(final Query query);

    Mono<DeviceDocument> saveDevice(DeviceDocument deviceDocument);

}
