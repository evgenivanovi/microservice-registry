package com.evgenivanovi.microservice.registry.mongodb.adapter.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Wither;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import reactor.core.publisher.Mono;

@Getter
@Setter

@Builder
@Wither

@NoArgsConstructor
@AllArgsConstructor

@Document(collection = DeviceDocument.DEVICE_COLLECTION_NAME)
public class DeviceDocument {

    static final String DEVICE_COLLECTION_NAME = "device";

    @Id
    private String id;

    @Indexed(unique = true)
    private String ssn;

    private String vendor;

    private String model;

    private String mac;

    private String ip;

    private String netmask;

    public Mono<DeviceDocument> toMono() {
        return Mono.just(this);
    }

    public static DeviceDocument empty() {
        return DeviceDocument.builder().build();
    }

    public static Mono<DeviceDocument> emptyMono() {
        return DeviceDocument.builder().build().toMono();
    }

    public static DeviceDocument dummy() {
        return DeviceDocument.builder().build();
    }

    public static Mono<DeviceDocument> dummyMono() {
        return DeviceDocument.builder().build().toMono();
    }

}
