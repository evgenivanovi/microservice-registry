package com.evgenivanovi.microservice.registry.mongodb.adapter.service;

import com.evgenivanovi.microservice.registry.domain.model.DeviceInfo;
import com.evgenivanovi.microservice.registry.domain.model.DeviceSaver;
import com.evgenivanovi.microservice.registry.mongodb.adapter.model.DeviceDocument;
import com.evgenivanovi.microservice.registry.mongodb.adapter.model.DeviceIndex;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
public class DeviceSavingService implements DeviceSaver {

    private final DeviceIndex deviceIndex;

    @Override
    public Mono<DeviceInfo> saveDevice(final DeviceInfo deviceInfo) {
        return deviceIndex.saveDevice(DeviceSavingService.toDeviceDocument(deviceInfo))
                .map(DeviceSavingService::toDeviceInfo);
    }

    private static DeviceInfo toDeviceInfo(final DeviceDocument deviceDocument) {
        return DeviceInfo.builder()
                .id(deviceDocument.getId())
                .ssn(deviceDocument.getSsn())
                .vendor(deviceDocument.getVendor())
                .model(deviceDocument.getModel())
                .mac(deviceDocument.getMac())
                .ip(deviceDocument.getIp())
                .netmask(deviceDocument.getNetmask())
                .build();
    }

    private static DeviceDocument toDeviceDocument(final DeviceInfo deviceInfo) {
        return DeviceDocument.builder()
                .id(deviceInfo.getId())
                .ssn(deviceInfo.getSsn())
                .vendor(deviceInfo.getVendor())
                .model(deviceInfo.getModel())
                .mac(deviceInfo.getMac())
                .ip(deviceInfo.getIp())
                .netmask(deviceInfo.getNetmask())
                .build();
    }

}
