package com.evgenivanovi.microservice.registry.app.configuration;

import com.evgenivanovi.microservice.registry.domain.model.DeviceLoader;
import com.evgenivanovi.microservice.registry.domain.model.DeviceSaver;
import com.evgenivanovi.microservice.registry.mongodb.adapter.model.DeviceIndex;
import com.evgenivanovi.microservice.registry.mongodb.adapter.service.DeviceLoadingService;
import com.evgenivanovi.microservice.registry.mongodb.adapter.service.DeviceSavingService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MongoAdapterInfrastructureConfiguration {

    @Bean
    DeviceLoader deviceLoader(final DeviceIndex deviceIndex) {
        return new DeviceLoadingService(deviceIndex);
    }

    @Bean
    DeviceSaver deviceSaver(final DeviceIndex deviceIndex) {
        return new DeviceSavingService(deviceIndex);
    }

}
