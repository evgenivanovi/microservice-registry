package com.evgenivanovi.microservice.registry.app.configuration;

import com.evgenivanovi.microservice.registry.device.config.infrastructure.model.WebDeviceConfigProperties;
import com.evgenivanovi.microservice.registry.device.config.infrastructure.service.DeviceConfigLoadingService;
import com.evgenivanovi.microservice.registry.domain.model.DeviceConfigLoader;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class DeviceConfigInfrastructureConfiguration {

    @Bean
    @ConfigurationProperties(prefix = "microservice.configuration")
    WebDeviceConfigProperties webDeviceConfigProperties() {
        return new WebDeviceConfigProperties();
    }

    @Bean
    WebClient.Builder webClientBuilder(final WebDeviceConfigProperties webDeviceConfigProperties) {
        return WebClient.builder().baseUrl(webDeviceConfigProperties.getUrl());
    }

    @Bean
    DeviceConfigLoader deviceConfigLoader(final WebClient.Builder webClientBuilder) {
        return new DeviceConfigLoadingService(webClientBuilder);
    }

}
