package com.evgenivanovi.microservice.registry.app.configuration;

import com.evgenivanovi.microservice.registry.mongodb.adapter.model.DeviceIndex;
import com.evgenivanovi.microservice.registry.mongodb.infrastructure.service.DeviceIndexService;
import com.mongodb.reactivestreams.client.MongoClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;

@Configuration
public class MongoInfrastructureConfiguration {

    private static final String MONGO_DATABASE_NAME = "db";

    @Bean
    ReactiveMongoTemplate reactiveMongoTemplate(final MongoClient mongoClient) {
        return new ReactiveMongoTemplate(mongoClient, MONGO_DATABASE_NAME);
    }

    @Bean
    DeviceIndex deviceIndex(final ReactiveMongoTemplate reactiveMongoTemplate) {
        return new DeviceIndexService(reactiveMongoTemplate);
    }

}
