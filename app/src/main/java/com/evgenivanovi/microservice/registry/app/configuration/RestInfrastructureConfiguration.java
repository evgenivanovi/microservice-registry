package com.evgenivanovi.microservice.registry.app.configuration;

import com.evgenivanovi.microservice.registry.rest.infrastructure.controller.ControllerErrorHandler;
import com.evgenivanovi.microservice.registry.rest.infrastructure.controller.DeviceController;
import com.evgenivanovi.microservice.registry.rest.port.model.DevicePort;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RestInfrastructureConfiguration {

    @Bean
    DeviceController registryController(final DevicePort devicePort) {
        return new DeviceController(devicePort);
    }

    @Bean
    ControllerErrorHandler controllerErrorHandler() {
        return new ControllerErrorHandler();
    }

}
