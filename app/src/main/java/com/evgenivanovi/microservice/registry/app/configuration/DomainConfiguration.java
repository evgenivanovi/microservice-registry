package com.evgenivanovi.microservice.registry.app.configuration;

import com.evgenivanovi.microservice.registry.domain.model.DeviceConfigLoader;
import com.evgenivanovi.microservice.registry.domain.model.DeviceLoader;
import com.evgenivanovi.microservice.registry.domain.model.DeviceManager;
import com.evgenivanovi.microservice.registry.domain.model.DeviceSaver;
import com.evgenivanovi.microservice.registry.domain.service.DeviceManagerService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DomainConfiguration {

    @Bean
    DeviceManager deviceManager(final DeviceLoader deviceLoader,
                                final DeviceSaver deviceSaver,
                                final DeviceConfigLoader deviceConfigLoader) {
        return new DeviceManagerService(deviceLoader, deviceSaver, deviceConfigLoader);
    }

}
