package com.evgenivanovi.microservice.registry.app.configuration;

import com.evgenivanovi.microservice.registry.domain.model.DeviceManager;
import com.evgenivanovi.microservice.registry.rest.port.model.DevicePort;
import com.evgenivanovi.microservice.registry.rest.port.service.DevicePortService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RestPortConfiguration {

    @Bean
    DevicePort registryPort(final DeviceManager deviceManager) {
        return new DevicePortService(deviceManager);
    }

}
