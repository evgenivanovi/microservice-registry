package com.evgenivanovi.microservice.registry.rest.port.model;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface DevicePort {

    Flux<DeviceItem> searchAllDevices();

    Flux<DeviceItem> searchDevices(DeviceSearchFilter deviceSearchFilter);

    Mono<DeviceItem> searchDevice(String id);

    Mono<DeviceItem> addDevice(DeviceDocument deviceDocument);

}
