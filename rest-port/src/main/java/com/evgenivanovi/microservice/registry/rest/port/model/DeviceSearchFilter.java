package com.evgenivanovi.microservice.registry.rest.port.model;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
public class DeviceSearchFilter {

    private String vendor;
    private String model;

    public boolean isEmpty() {
        return (StringUtils.isBlank(this.vendor))
                && (StringUtils.isBlank(this.model));
    }

}
