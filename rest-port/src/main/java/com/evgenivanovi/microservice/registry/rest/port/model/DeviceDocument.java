package com.evgenivanovi.microservice.registry.rest.port.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeviceDocument {

    private String ssn;
    private String vendor;
    private String model;
    private String mac;

}
