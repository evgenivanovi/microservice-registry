package com.evgenivanovi.microservice.registry.rest.port.service;

import com.evgenivanovi.microservice.registry.domain.model.Device;
import com.evgenivanovi.microservice.registry.domain.model.DeviceFilter;
import com.evgenivanovi.microservice.registry.domain.model.DeviceInfo;
import com.evgenivanovi.microservice.registry.domain.model.DeviceManager;
import com.evgenivanovi.microservice.registry.rest.port.model.DeviceDocument;
import com.evgenivanovi.microservice.registry.rest.port.model.DeviceSearchFilter;
import com.evgenivanovi.microservice.registry.rest.port.model.DevicePort;
import com.evgenivanovi.microservice.registry.rest.port.model.DeviceItem;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@RequiredArgsConstructor
public class DevicePortService implements DevicePort {

    private final DeviceManager deviceManager;

    @Override
    public Flux<DeviceItem> searchAllDevices() {
        log.info("Registry Port: Search All Devices");
        return deviceManager.searchAllDevices()
                .map(DevicePortService::toDeviceItem);
    }

    @Override
    public Flux<DeviceItem> searchDevices(final DeviceSearchFilter deviceSearchFilter) {
        log.info("Registry Port: Search Filter Devices");
        return deviceManager.searchDevices(toDeviceFilter(deviceSearchFilter))
                .map(DevicePortService::toDeviceItem);
    }

    @Override
    public Mono<DeviceItem> searchDevice(final String id) {
        log.info("Registry Port: Search Device By ID");
        return deviceManager.searchSingleDevice(id)
                .map(DevicePortService::toDeviceItem);
    }

    @Override
    public Mono<DeviceItem> addDevice(final DeviceDocument deviceDocument) {
        log.info("Registry Port: Add Device");
        return deviceManager.addDevice(toDevice(deviceDocument))
                .map(DevicePortService::toDeviceItem);
    }

    private static Device toDevice(final DeviceDocument deviceDocument) {
        return Device.builder()
                .ssn(deviceDocument.getSsn())
                .vendor(deviceDocument.getVendor())
                .model(deviceDocument.getModel())
                .mac(deviceDocument.getMac())
                .build();
    }

    private static DeviceItem toDeviceItem(final DeviceInfo deviceInfo) {
        return DeviceItem.builder()
                .id(deviceInfo.getId())
                .ssn(deviceInfo.getSsn())
                .vendor(deviceInfo.getVendor())
                .model(deviceInfo.getModel())
                .mac(deviceInfo.getMac())
                .ip(deviceInfo.getIp())
                .netmask(deviceInfo.getNetmask())
                .build();
    }

    private static DeviceFilter toDeviceFilter(final DeviceSearchFilter deviceSearchFilter) {
        return DeviceFilter.builder()
                .vendor(deviceSearchFilter.getVendor())
                .model(deviceSearchFilter.getModel())
                .build();
    }

}
