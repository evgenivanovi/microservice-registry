package com.evgenivanovi.microservice.registry.rest.port.model;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Wither;
import reactor.core.publisher.Mono;

@Getter
@Wither
@Builder
public class DeviceItem {

    private String id;
    private String ssn;
    private String vendor;
    private String model;
    private String mac;
    private String ip;
    private String netmask;

    public Mono<DeviceItem> toMono() {
        return Mono.just(this);
    }

    public static Mono<DeviceItem> emptyMono() {
        return DeviceItem.empty().toMono();
    }

    public static DeviceItem empty() {
        return DeviceItem.builder().build();
    }

}
