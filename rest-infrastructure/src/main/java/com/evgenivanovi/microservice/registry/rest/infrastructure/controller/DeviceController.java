package com.evgenivanovi.microservice.registry.rest.infrastructure.controller;

import com.evgenivanovi.microservice.registry.rest.port.model.DeviceDocument;
import com.evgenivanovi.microservice.registry.rest.port.model.DeviceSearchFilter;
import com.evgenivanovi.microservice.registry.rest.port.model.DevicePort;
import com.evgenivanovi.microservice.registry.rest.port.model.DeviceItem;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = DeviceController.REGISTRY_PATH)
@RequiredArgsConstructor
public class DeviceController {

    static final String REGISTRY_PATH = "/api/service/registry";
    static final String ID_PATH = "/{id}";

    private final DevicePort devicePort;

    @RequestMapping(
            method = RequestMethod.POST,
            produces = {
                    MediaType.APPLICATION_JSON_VALUE
            },
            consumes = {
                    MediaType.APPLICATION_JSON_VALUE,
            }
    )
    public Mono<DeviceItem> addDevice(@RequestBody final DeviceDocument deviceDocument) {
        return devicePort.addDevice(deviceDocument);
    }

    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_STREAM_JSON_VALUE,
            consumes = MediaType.APPLICATION_STREAM_JSON_VALUE
    )
    public Flux<DeviceItem> searchDevices(final DeviceSearchFilter deviceSearchFilter) {
        if (deviceSearchFilter.isEmpty()) {
            return devicePort.searchAllDevices();
        } else {
            return devicePort.searchDevices(deviceSearchFilter);
        }
    }

    @RequestMapping(
            method = RequestMethod.GET, path = ID_PATH,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public Mono<DeviceItem> searchDevice(@PathVariable(name = "id") final String id) {
        return devicePort.searchDevice(id);
    }

}
