package com.evgenivanovi.microservice.registry.rest.infrastructure.controller;

import lombok.Builder;
import lombok.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import reactor.core.publisher.Mono;

@RestControllerAdvice
public class ControllerErrorHandler {

    private static final String DEFAULT_MESSAGE = "Please, do not try to hack me!";

    @ExceptionHandler(value = Exception.class)
    public Mono<ResponseEntity<Object>> handle() {
        return Mono.just(ErrorDescription.of(DEFAULT_MESSAGE))
                .map(ControllerErrorHandler::response);
    }

    private static <T> ResponseEntity<T> response(final T value) {
        return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT)
                .contentType(MediaType.APPLICATION_STREAM_JSON)
                .body(value);
    }

    @Value
    @Builder
    static class ErrorDescription {
        private String message;

        static ErrorDescription of(final String message) {
            return new ErrorDescription(message);
        }
    }

}
