package com.evgenivanovi.microservice.registry.mongodb.infrastructure.service;

import com.evgenivanovi.microservice.registry.mongodb.adapter.model.DeviceDocument;
import com.evgenivanovi.microservice.registry.mongodb.adapter.model.DeviceIndex;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataMongoTest
@EnableAutoConfiguration
@Import(value = DeviceIndexServiceTest.Configuration.class)
class DeviceIndexServiceTest {

    @Autowired
    private ReactiveMongoTemplate mongoTemplate;

    @Autowired
    private DeviceIndexService testee;

    @BeforeEach
    void assume() {
        Assumptions.assumeTrue(Objects.nonNull(mongoTemplate));
        Assumptions.assumeTrue(Objects.nonNull(testee));
    }

    @BeforeEach
    void init() {
        StepVerifier.create(mongoTemplate.dropCollection(DeviceDocument.class))
                .verifyComplete();

        Flux<DeviceDocument> insertAll = mongoTemplate
                .insertAll(Flux.fromIterable(generate(10)).collectList());

        StepVerifier.create(insertAll)
                .expectNextCount(10)
                .verifyComplete();
    }

    @Test
    void ShouldFindAllDevices() {
        // given
        int expectedSize = 10;
        Query query = new Query();

        // when
        List<DeviceDocument> actualDeviceDocuments = testee.searchDevices(query)
                .collectList().block();

        // then
        assertNotNull(actualDeviceDocuments);
        assertEquals(actualDeviceDocuments.size(), expectedSize);
    }

    @Test
    void WhenDeviceDoesExists_ShouldFindDeviceBySerialNumberQuery() {
        // given
        DeviceDocument expectedDeviceDocument = DeviceDocument.empty()
                .withSsn("SSN_1")
                .withVendor("VENDOR_1")
                .withModel("MODEL_1")
                .withMac("MAC_1")
                .withIp("IP_1")
                .withNetmask("NETMASK_1");

        Query query = Query.query(Criteria.where("ssn").is("SSN_1"));

        // when
        DeviceDocument actualDeviceDocument = testee.searchDevice(query)
                .block();

        // then
        assertAll(
                () -> assertNotNull(actualDeviceDocument),
                () -> assertEquals(expectedDeviceDocument.getSsn(), actualDeviceDocument.getSsn()),
                () -> assertEquals(expectedDeviceDocument.getVendor(), actualDeviceDocument.getVendor()),
                () -> assertEquals(expectedDeviceDocument.getModel(), actualDeviceDocument.getModel()),
                () -> assertEquals(expectedDeviceDocument.getMac(), actualDeviceDocument.getMac()),
                () -> assertEquals(expectedDeviceDocument.getIp(), actualDeviceDocument.getIp()),
                () -> assertEquals(expectedDeviceDocument.getNetmask(), actualDeviceDocument.getNetmask())
        );
    }

    @Test
    void WhenDeviceDoesNotExist_ShouldNotFindDeviceBySerialNumberQuery() {
        // given
        DeviceDocument expectedDeviceDocument = null;

        Query query = Query.query(Criteria.where("ssn").is("SSN_11"));

        // when
        DeviceDocument actualDeviceDocument = testee.searchDevice(query)
                .block();

        // then
        assertEquals(expectedDeviceDocument, actualDeviceDocument);
    }

    @Test
    void ShouldSaveDevice() {
        // given
        DeviceDocument deviceDocument = DeviceDocument.empty().withSsn("SSN_11");

        // when
        DeviceDocument actualDeviceDocument = testee.saveDevice(deviceDocument)
                .block();

        // then
        assertAll(
                () -> assertNotNull(actualDeviceDocument),
                () -> assertNotNull(actualDeviceDocument.getId())
        );
    }

    @org.springframework.context.annotation.Configuration
    static class Configuration {

        @Bean
        DeviceIndex deviceIndex(@Autowired final ReactiveMongoTemplate reactiveMongoTemplate) {
            return new DeviceIndexService(reactiveMongoTemplate);
        }

    }

    private static Collection<DeviceDocument> generate(final int count) {
        return IntStream.range(0, count)
                .boxed()
                .map(DeviceIndexServiceTest::createDeviceDocument)
                .collect(Collectors.toList());
    }

    private static DeviceDocument createDeviceDocument(final int value) {
        return DeviceDocument.empty()
                .withSsn("SSN_" + value)
                .withVendor("VENDOR_" + value)
                .withModel("MODEL_" + value)
                .withMac("MAC_" + value)
                .withIp("IP_" + value)
                .withNetmask("NETMASK_" + value);
    }

}