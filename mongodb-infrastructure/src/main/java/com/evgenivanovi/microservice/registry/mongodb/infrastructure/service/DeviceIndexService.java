package com.evgenivanovi.microservice.registry.mongodb.infrastructure.service;

import com.evgenivanovi.microservice.registry.mongodb.adapter.model.DeviceDocument;
import com.evgenivanovi.microservice.registry.mongodb.adapter.model.DeviceIndex;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@RequiredArgsConstructor
public class DeviceIndexService implements DeviceIndex {

    private final ReactiveMongoTemplate mongoTemplate;

    @Override
    public Flux<DeviceDocument> searchDevices(final Query query) {
        return mongoTemplate.find(query, DeviceDocument.class);
    }

    @Override
    public Mono<DeviceDocument> searchDevice(final Query query) {
        return mongoTemplate.findOne(query, DeviceDocument.class);
    }

    @Override
    public Mono<DeviceDocument> saveDevice(final DeviceDocument deviceDocument) {
        return mongoTemplate.save(deviceDocument);
    }

}
